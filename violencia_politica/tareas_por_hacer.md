# Tareas para proyecto final

* [X] Reestructurar tweets
* [ ] Crear base de datos en mongo
    * [ ] Carga de tweets y replies en base de datos
    * [ ] Consultas utiles
* [ ] Revision de Lexicon para español
* [ ] Decidir campos a utilizar
* [ ] Hacer preprocesamiento de replies
* [ ] Utilizar biblioteca para analisis de sentimientos
* [ ] Entrega 
